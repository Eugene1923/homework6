<?php 

$users = [
    [
        'name' => 'Alex',
        'group' => 'teacher',
        'email' => 'alex@gmail.com',
        'phone' => '222-333-431'
    ],
    [
        'name' => 'Eugene',
        'group' => 'student',
        'email' => 'eugene@gmail.com',
        'phone' => '232-323-441'
    ],
    [
        'name' => 'Nick',
        'group' => 'admin',
        'email' => 'nick@gmail.com',
        'phone' => '223-334-411'
    ],
    [
        'name' => 'Ronald',
        'group' => 'teacher',
        'email' => 'ronald@gmail.com',
        'phone' => '543-336-481'
    ],
    [
        'name' => 'Kate',
        'group' => 'student',
        'email' => 'kate@gmail.com',
        'phone' => '666-311-437'
    ],
    [
        'name' => 'Tim',
        'group' => 'admin',
        'email' => 'tim@gmail.com',
        'phone' => '111-222-444'
    ],
    [
        'name' => 'James',
        'group' => 'teacher',
        'email' => 'james@gmail.com',
        'phone' => '555-777-888'
    ],
    [
        'name' => 'Henry',
        'group' => 'student',
        'email' => 'henry@gmail.com',
        'phone' => '888-111-999'
    ],
    [
        'name' => 'John',
        'group' => 'admin',
        'email' => 'john@gmail.com',
        'phone' => '123-321-433'
    ],
    [
        'name' => 'Robert',
        'group' => 'student',
        'email' => 'robert@gmail.com',
        'phone' => '456-175-961'
    ]
];

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Homework</title>
    <!-- CSS only -->
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
</head>
<body>
<table class="table caption-top">
<thead class="table-dark">
    <tr>
      <th scope="col">Name</th>
      <th scope="col">Group</th>
      <th scope="col">Email</th>
      <th scope="col">Phone</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row"><?php 
    foreach($users as $user){
        if($user['group'] === 'teacher'){
             echo $user['name'] . '<br>';
    }
}?></th>
      <th><?php 
    foreach($users as $user){
        if($user['group'] === 'teacher'){
             echo $user['group'] . '<br>';
    }
}?></th>
      <th><?php 
    foreach($users as $user){
        if($user['group'] === 'teacher'){
             echo $user['email'] . '<br>';
    }
}?></th>
      <th><?php 
    foreach($users as $user){
        if($user['group'] === 'teacher'){
             echo $user['phone'] . '<br>';
    }
}?></th>
    </tr>
  </tbody>
</table>
<br>
<br>
<a href="index.php">Homepage</a>

<!-- JavaScript Bundle with Popper -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
</body>
</html>

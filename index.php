<?php

$age = $_GET['age'];
$action = $_GET['status'];

$users = [
    [
        'name' => 'Alex',
        'group' => 'teacher',
        'email' => 'alex@gmail.com',
        'phone' => '222-333-431'
    ],
    [
        'name' => 'Eugene',
        'group' => 'student',
        'email' => 'eugene@gmail.com',
        'phone' => '232-323-441'
    ],
    [
        'name' => 'Nick',
        'group' => 'admin',
        'email' => 'nick@gmail.com',
        'phone' => '223-334-411'
    ],
    [
        'name' => 'Ronald',
        'group' => 'teacher',
        'email' => 'ronald@gmail.com',
        'phone' => '543-336-481'
    ],
    [
        'name' => 'Kate',
        'group' => 'student',
        'email' => 'kate@gmail.com',
        'phone' => '666-311-437'
    ],
    [
        'name' => 'Tim',
        'group' => 'admin',
        'email' => 'tim@gmail.com',
        'phone' => '111-222-444'
    ],
    [
        'name' => 'James',
        'group' => 'teacher',
        'email' => 'james@gmail.com',
        'phone' => '555-777-888'
    ],
    [
        'name' => 'Henry',
        'group' => 'student',
        'email' => 'henry@gmail.com',
        'phone' => '888-111-999'
    ],
    [
        'name' => 'John',
        'group' => 'admin',
        'email' => 'john@gmail.com',
        'phone' => '123-321-433'
    ],
    [
        'name' => 'Robert',
        'group' => 'student',
        'email' => 'robert@gmail.com',
        'phone' => '456-175-961'
    ]
];

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>homework</title>
       <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
</head>
<body>
    <form method="GET" action="index.php" class="row g-3 needs-validation">
    <div class="col-md-3">
    <label for="validationCustom01" class="form-label">ФИО</label>
    <input type="text" class="form-control" id="validationCustom01" name="name">
    <br>
    <select class="form-select form-select-sm" aria-label=".form-select-sm example" name="status">
    <option selected>Выберите статус</option>
    <option value="student">student</option>
    <option value="teacher">teacher</option>
    <option value="admin">admin</option>
</select>
  </div>
  <div class="col-md-1">
    <label for="validationCustom02" class="form-label">Возраст</label>
    <input type="text" class="form-control" id="validationCustom02" name="age">
  </div>
  <div class="col-md-3">
    <label for="validationCustomUsername" class="form-label">email</label>
    <div class="input-group has-validation">
      <span class="input-group-text" id="inputGroupPrepend">@</span>
      <input type="text" class="form-control" id="validationCustomUsername" aria-describedby="inputGroupPrepend" name="email">
    </div>
  </div>
  <div class="col-md-2">
    <label for="validationCustom04" class="form-label">phone</label>
    <input type="text" class="form-control" id="validationCustom03" name="phone">
  </div>
  <div class="col-12">
    <button class="btn btn-primary" type="submit">Зарегистрироваться</button>
  </div>
</form>

<?php
switch($age){
    case ($age < 18) :
        echo "Извините,вы слишком молоды";
        break;
}
if ($action === 'student' && $age >= 18){
    echo "Поздравляем, Вы успешно зарегистрированы и добавлены в список: Студентов";
}
if ($action === 'teacher' && $age >= 18){
    echo "Поздравляем, Вы успешно зарегистрированы и добавлены в список: Преподавателей";
}
if($action === 'admin' && $age >= 18){
    echo "Поздравляем, Вы успешно зарегистрированы и добавлены в список: Администраторов";
}

?>
<br>
<br>
<a href ="student.php">Student page</a>
<br>
<a href ="teacher.php">Teacher page</a>
<br>
<a href ="admin.php">Admin page</a>
<br>
<br>
<table class="table caption-top">
<thead class="table-dark">
    <tr>
      <th scope="col">Name</th>
      <th scope="col">Group</th>
      <th scope="col">Email</th>
      <th scope="col">Phone</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row"><?php foreach($users as $user){
          echo $user['name'] . '<br>';
      }?></th>
      <th><?php foreach($users as $user){
          echo $user['group'] . '<br>';
      }?></th>
      <th><?php foreach($users as $user){
          echo $user['email'] . '<br>';
      }?></th>
      <th><?php foreach($users as $user){
          echo $user['phone'] . '<br>';
      }?></th>
    </tr>
  </tbody>
</table>

    <!-- JavaScript Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
</body>
</html>
